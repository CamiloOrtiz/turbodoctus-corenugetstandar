﻿using App.Config.Dependencies;
using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.Models;
using App.Infrastructure.Database.SecurityEntities;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace App.IdentityServer
{
    public class Startup
    {

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddScoped<Doctus_PlantillaNetCoreContext, Doctus_PlantillaNetCoreContext>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<Doctus_PlantillaNetCoreContext>();


            // configure identity server with in-memory stores, keys, clients and scopes
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(identityConfig.GetIdentityResources())
                .AddInMemoryApiResources(identityConfig.GetApiResources())
                .AddInMemoryClients(identityConfig.GetClients())
                .AddTestUsers(identityConfig.GetUsers())
                .AddAspNetIdentity<User>();
            services.AddMvc();


            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPassword>();
            services.AddTransient<IProfileService, IdentityClaimsProfileService>();


            ConfigureDatabase(services);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
        private void ConfigureDatabase(IServiceCollection services)
        {
            //configure database
            services.AddDbContext<Doctus_PlantillaNetCoreContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseSqlServer("data source=MED-SQLSERVER;Database=Doctus_PlantillaNetCore3;user id=pruebasjuan;password=Raul.99;", x => { x.MigrationsAssembly("App.Infrastructure"); x.UseRowNumberForPaging(); });
            });
        }
        public static void CreateDatabase(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<Doctus_PlantillaNetCoreContext>();
                    DbInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    throw ex;
                    //var logger = services.GetRequiredService<ILogger<app>>();
                    //logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }
        }
    }
}

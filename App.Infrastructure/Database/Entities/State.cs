﻿using System;
using System.Collections.Generic;

namespace App.Infrastructure.database.Entities
{
    public partial class State
    {
        public State()
        {
            Activity = new HashSet<Activity>();
        }

        public int StateId { get; set; }
        public string StateName { get; set; }

        public virtual ICollection<Activity> Activity { get; set; }
    }
}

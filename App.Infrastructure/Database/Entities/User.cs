﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Infrastructure.Database.SecurityEntities
{
    public class User : IdentityUser<int>
    {
        public User()
             : base()
        {
            //Claim = new HashSet<Claim>();
        }

        public User(string username)
            : base(username)
        {
            //Claim = new HashSet<Claim>();
        }

        [NotMapped]
        public int UserId
        {
            get { return this.Id; }
            set { this.Id = UserId; }
        }

        public bool IsBlocked { get; set; }

        //public virtual Role Role { get; set; }
        public virtual ICollection<IdentityUserRole<int>> Roles { get; set; }
        public virtual ICollection<IdentityUserClaim<int>> Claims { get; set; }



        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string Token { get; set; }
        public bool ExternalUser { get; set; }
        public string Identification { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public int? GenderId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime CreationDate { get; set; }


    }
}

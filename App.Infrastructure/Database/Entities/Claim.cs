﻿using App.Infrastructure.Database.SecurityEntities;
using System;
using System.Collections.Generic;

namespace App.Infrastructure.Database.Entities
{
    public partial class Claim
    {
        public int ClaimId { get; set; }
        public int UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        public virtual User User { get; set; }
    }
}

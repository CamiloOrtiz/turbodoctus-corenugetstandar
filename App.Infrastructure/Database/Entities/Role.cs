﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Database.SecurityEntities
{
    public class Role : IdentityRole<int>
    {
        public Role()
            : base()
        {
            //UserRole = new HashSet<IdentityUserRole<int>>();
        }

        public Role(string roleName) : base(roleName)
        {
            //UserRole = new HashSet<IdentityUserRole<int>>();
        }

        [NotMapped]
        public int RoleId
        {
            get { return this.Id; }
            set { this.Id = RoleId; }
        }

        public virtual ICollection<IdentityUserRole<int>> Users { get; set; }

        public virtual ICollection<IdentityRoleClaim<int>> Claims { get; set; }


    }

}


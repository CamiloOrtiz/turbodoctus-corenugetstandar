﻿using App.Infrastructure.Database.SecurityEntities;
using System;
using System.Collections.Generic;

namespace App.Infrastructure.database.Entities
{
    public partial class Activity
    {
        public int ActivityId { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Title { get; set; }
        public int? EstimatedHours { get; set; }
        public int? UserId { get; set; }
        public int? StateId { get; set; }

        public virtual State State { get; set; }
        public virtual User User { get; set; }
    }
}

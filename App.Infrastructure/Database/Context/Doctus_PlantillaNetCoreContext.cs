﻿using System;
using App.Infrastructure.Database.Entities;
using App.Infrastructure.Database.SecurityEntities;
using IdentityServer4.EntityFramework.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace App.Infrastructure.database.Entities
{
    public partial class Doctus_PlantillaNetCoreContext : IdentityDbContext<User, Role, int>
    {

        public Doctus_PlantillaNetCoreContext(DbContextOptions<Doctus_PlantillaNetCoreContext> options) : base(options)
        {
        }

        public virtual DbSet<Activity> Activity { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<User> User { get; set; }
        ////public DbSet<Client> Clients { get; set; }
        ////public DbSet<IdentityResource> IdentityResources { get; set; }
        //public virtual DbSet<Claim> Claim { get; set; }
        ////public DbSet<ApiResource> ApiResources { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasMany(u => u.Claims).WithOne().HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<User>().HasMany(u => u.Roles).WithOne().HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>().HasMany(r => r.Claims).WithOne().HasForeignKey(c => c.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Role>().HasMany(r => r.Users).WithOne().HasForeignKey(r => r.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);


            //modelBuilder.Entity<Role>().HasMany(r => r.Claims).WithOne().HasForeignKey(c => c.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);



            modelBuilder.Entity<Activity>(entity =>
            {
                entity.ToTable("Activity", "Activities");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Title)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Activity)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("Fk_Activity_State");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.Activity)
                //    .HasForeignKey(d => d.UserId)
                //    .HasConstraintName("FK_Activity_AspNetUsers");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Roles");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.NormalizedName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ConcurrencyStamp)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("State", "Activities");

                entity.Property(e => e.StateName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users");


                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.PasswordHash).IsRequired();

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.PasswordHash).IsRequired();

                entity.Property(e => e.Phone)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.GenderId)
                    .IsRequired();

                

                //entity.HasOne(d => d.Role)
                //    .WithMany(p => p.User)
                //    .HasForeignKey(d => d.RoleId)
                //    .HasConstraintName("fk_User_Role");
            });
        }
    }
}

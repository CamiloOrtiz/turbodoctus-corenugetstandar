﻿using App.Infrastructure.database.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Infrastructure.Database.Models
{
    public static class DbInitializer
    {
        public static void Initialize(Doctus_PlantillaNetCoreContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}

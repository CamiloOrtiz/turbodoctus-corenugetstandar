﻿using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.SecurityEntities;
using Doctus.Base.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repositories.EntityFramework.Users
{
    public interface IUserRepository : IBaseCRUDRepository<User>
    {
         Task<User> FindByNameAsync(String UserName);
    }
    public class UserRepository : BaseCRUDRepository<User>, IUserRepository
    {
        private readonly UserManager<User> _userManager;
        public UserRepository(Doctus_PlantillaNetCoreContext database, UserManager<User> userManager)
    : base(database)
        {
            _userManager = userManager;
        }

        public async Task<User> FindByNameAsync(String UserName)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(UserName);
                return user;
            }
            catch (Exception ex)
            {
                var ess = ex;
                throw;
            }
            
        }


    }
}

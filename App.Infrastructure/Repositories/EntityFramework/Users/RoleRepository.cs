﻿using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.SecurityEntities;
using Doctus.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Infrastructure.Repositories.EntityFramework.Users
{
    public interface IRoleRepository : IBaseCRUDRepository<Role>
    {

    }
    public class RoleRepository : BaseCRUDRepository<Role>, IRoleRepository
    {
        public RoleRepository(Doctus_PlantillaNetCoreContext database)
    : base(database)
        {

        }

    }
}

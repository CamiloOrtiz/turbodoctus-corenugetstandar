﻿using App.Infrastructure.database.Entities;
using Doctus.Base.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Infrastructure.Repositories.EntityFramework.Activities
{
    public interface IStateRepository : IBaseCRUDRepository<State>
    {

    }
    public class StateRepository : BaseCRUDRepository<State>, IStateRepository
    {
        public StateRepository(Doctus_PlantillaNetCoreContext database)
    : base(database)
        {

        }
    }
}

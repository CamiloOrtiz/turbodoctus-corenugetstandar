﻿using System.Linq;
using App.Infrastructure.database.Entities;
using Doctus.Base.Repositories;

namespace App.Infrastructure.Repositories.EntityFramework.Activities
{
    public interface IActivityRepository : IBaseCRUDRepository<Activity>
    {

    }
    public class ActivityRepository : BaseCRUDRepository<Activity>, IActivityRepository
    {
        public ActivityRepository(Doctus_PlantillaNetCoreContext database)
    : base(database)
        {

        }

    }
   
    
}

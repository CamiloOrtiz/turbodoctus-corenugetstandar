﻿using System;
using System.Collections.Generic;

namespace App.Infrastructure.Model_2
{
    public partial class State
    {
        public State()
        {
            Activity = new HashSet<Activity>();
        }

        public int StateId { get; set; }
        public string StateName { get; set; }

        public ICollection<Activity> Activity { get; set; }
    }
}

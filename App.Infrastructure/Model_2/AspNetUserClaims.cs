﻿using System;
using System.Collections.Generic;

namespace App.Infrastructure.Model_2
{
    public partial class AspNetUserClaims
    {
        public int Id { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public int UserId { get; set; }

        public AspNetUsers User { get; set; }
    }
}

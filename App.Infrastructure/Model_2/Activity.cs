﻿using System;
using System.Collections.Generic;

namespace App.Infrastructure.Model_2
{
    public partial class Activity
    {
        public int ActivityId { get; set; }
        public DateTime? CreationDate { get; set; }
        public string Description { get; set; }
        public int? EstimatedHours { get; set; }
        public int? StateId { get; set; }
        public string Title { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? UserId { get; set; }

        public State State { get; set; }
        public AspNetUsers User { get; set; }
    }
}

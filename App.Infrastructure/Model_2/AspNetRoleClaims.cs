﻿using System;
using System.Collections.Generic;

namespace App.Infrastructure.Model_2
{
    public partial class AspNetRoleClaims
    {
        public int Id { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public int RoleId { get; set; }

        public AspNetRoles Role { get; set; }
    }
}

﻿using Doctus.Base.Cache;
using App.Common.Classes.DTO.Users;
using Doctus.Exceptions;
using App.Common.Resources;
using App.Common.Resources.Users;
using App.Infrastructure.Database.SecurityEntities;
using App.Infrastructure.Repositories.EntityFramework.Users;
using AspNet.Security.OpenIdConnect.Primitives;
using AutoMapper;
using Doctus.Base.Services;
using Doctus.Validator;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace App.Domain.Services.Users
{
    public interface IUserService : IBaseService<UserDTO>
    {
        Task<UserDTO> Login(OpenIdConnectRequest userd, string url);
        Task<UserDTO> CreateUserAsync(UserDTO usr);
        Task<UserDTO> GetUserIdentityByEmailAsync(string email);
        Task<UserDTO> GetUserIdentityByUserNameAsync(string userName);
        Task<bool> CanSignInAsync(OpenIdConnectRequest request);
        Task<UserDTO> GetUserAsync(ClaimsPrincipal principal);
        Task<ClaimsPrincipal> CreateUserPrincipalAsync(OpenIdConnectRequest request);
        Task<IActionResult> RequestToken(string url, User usr);
        Task<UserDTO> FindByNameAsync(UserDTO userDTO);
    }
    public class UserService : BaseService<UserDTO, User>, IUserService
    {
        private IUserRepository _UserRepository;
        private IRoleRepository _roleRepository;
        private UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private IHttpContextAccessor _httpContext;
        IStringLocalizer<UserResource> _Localizer;
        private readonly RoleManager<Role> _roleManager;



        public UserService(IUserRepository repository, IMemoryCacheManager memoryCache,
            IMapper mapper, IServiceValidator<User> validation, IConfiguration configuration, UserManager<User> userManager, RoleManager<Role> roleManager, IRoleRepository roleRepository,
            SignInManager<User> signInManager, IStringLocalizer<UserResource> localizer, IRedisCache cache, IHttpContextAccessor context)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
            //IHttpContextAccessor context, IOptions< AppSettingsDTO > appSettings, IAuthService authService, IRoleRepository roleRepository, IStringLocalizer<UserResource> localizer, IStringLocalizer< GlobalResource > globalLocalizer) 
            _UserRepository = repository;
            _userManager = userManager;
            _roleRepository = roleRepository;
            _signInManager = signInManager;
            _Localizer = localizer;
            _httpContext = context;
            _roleManager = roleManager;

        }

        public async Task<UserDTO> GetUserIdentityByUserNameAsync(string userName)
        {
            return _mapperDependency.Map<UserDTO>(await _userManager.FindByNameAsync(userName));
        }

                public async Task<ClaimsPrincipal> CreateUserPrincipalAsync(OpenIdConnectRequest request)
        {
            var user = await _userManager.FindByNameAsync(request.Username);
            return await _signInManager.CreateUserPrincipalAsync(user);
        }


        public async Task<UserDTO> GetUserAsync(ClaimsPrincipal principal)
        {
            var user = await _userManager.GetUserAsync(principal);
            if (user != null)
            {
                return _mapperDependency.Map<UserDTO>(user);
            }
            else
            {
                List<string> messages = new List<string>() { string.Format(_Localizer["LoginError"]) };
                throw new ValidationServiceException(messages);
            }

        }

        public async Task<bool> CanSignInAsync(OpenIdConnectRequest request)
        {
            var user = await _userManager.FindByNameAsync(request.Username);
            return await _signInManager.CanSignInAsync(user);
        }

        public async Task<UserDTO>  Login(OpenIdConnectRequest userd, string url)
        {

            var user = await _UserRepository.FindByNameAsync(userd.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, userd.Password))
            {
                UserDTO entity = _mapperDependency.Map<UserDTO>(user);
                entity.Token = await RequestToken(url, user);
                return entity;
            }
            return null;
        }

        public async Task<UserDTO> GetUserIdentityByEmailAsync(string email)
        {
            return _mapperDependency.Map<UserDTO>(await _userManager.FindByEmailAsync(email));
        }

        
        public async Task<UserDTO> CreateUserAsync(UserDTO usr)
        {
            usr.CreationDate = Doctus.DateTimeHelper.DateTimeHelper.GetCurrentColombianTime();
            var entity = _mapperDependency.Map<User>(usr);
            //Prevalidacion
            _validator.PreInsert(entity);

            var role = await _roleManager.FindByIdAsync(usr.RoleId);

            string password = usr.PasswordHash;
            var result = await _userManager.CreateAsync(entity, password);
            if (role == null)
            {
                throw new ApplicationException("the role is not found");
            }
            else
            if (result.Succeeded)
            {
                await this._userManager.AddToRoleAsync(entity, role.Name);
                //await _userManager.AddClaimAsync(entity, new Claim("userName", entity.UserName));
                //await _userManager.AddClaimAsync(entity, new Claim("email", entity.Email));
                //await _userManager.AddClaimAsync(entity, new Claim("role", role.Name));
            }


            if (!result.Succeeded)
                throw new ValidationServiceException(result.Errors.Select(e => e.Description).ToList());

            var tempUser = await _userManager.FindByNameAsync(usr.UserName);


            if (!result.Succeeded)
            {
                //await DeleteUserAsync(tempUser);
                throw new ValidationServiceException(result.Errors.Select(e => e.Description).ToList());
            }
            // Log insert
            //PostValidacion
            _validator.PostInsert(entity);

            return _mapperDependency.Map<UserDTO>(tempUser);
        }

        private Task DeleteUserAsync(User tempUser)
        {
            throw new NotImplementedException();
        }


        public async Task<IActionResult> RequestToken(string url,User usr)
        {
            var disco = await DiscoveryClient.GetAsync(url);

            var tokenclient = new TokenClient(disco.TokenEndpoint, _configuration["Identity:ClientId"], _configuration["Identity:ClientSecret"]);
            var tokenResponse = await tokenclient.RequestResourceOwnerPasswordAsync(usr.UserName, usr.PasswordHash, _configuration["Identity:ApiName"]);
            

            if (tokenResponse.IsError)
            {
                return new JsonResult(tokenResponse.Error);
            }

            return new JsonResult(tokenResponse.Json);
        }

            public async Task<UserDTO> FindByNameAsync(UserDTO userDTO)
        {
            return _mapperDependency.Map<UserDTO>(await _UserRepository.FindByNameAsync(userDTO.UserName));
        }
        #region Validator

        public class USerValidator : BaseServiceValidator<User, UserResource>
        {
            IStringLocalizer<GlobalResource> _globalLocalizer;

            public USerValidator(IStringLocalizer<UserResource> localizer,
                IStringLocalizer<GlobalResource> globalLocalizer) : base(localizer)
            {
                _globalLocalizer = globalLocalizer;

            }

            #region Insert Rules

            public override void LoadPreInsertRules()
            {

            }

            public override void LoadPostInsertRules()
            {

            }

            #endregion

            #region Update Rules

            public override void LoadPreUpdateRules()
            {

            }

            public override void LoadPostUpdateRules()
            {

            }

            #endregion

            #region Delete Rules

            public override void LoadPreDeleteRules()
            {

            }

            public override void LoadPostDeleteRules()
            {

            }

            #endregion

            #region Advanced Validations



            #endregion
        }

        #endregion
    }
}

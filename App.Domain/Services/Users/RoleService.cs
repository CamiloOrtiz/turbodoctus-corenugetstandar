﻿using App.Common.Classes.DTO.Users;
using App.Common.Resources;
using App.Common.Resources.Users;
using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.SecurityEntities;
using App.Infrastructure.Repositories.EntityFramework.Users;
using AutoMapper;
using Doctus.Base.Cache;
using Doctus.Base.Services;
using Doctus.Validator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Domain.Services.Users
{
    public interface IRoleService : IBaseService<RoleDTO>
    {

    }
    public class RoleService : BaseService<RoleDTO, Role>, IRoleService
    {
        private IRoleRepository _RoleRepository;

        public RoleService(IRoleRepository repository, IMemoryCacheManager memoryCache,
            IMapper mapper, IServiceValidator<Role> validation, IConfiguration configuration, IRedisCache cache)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
            _RoleRepository = repository;

        }


    }

    #region Validator

    public class RoleValidator : BaseServiceValidator<Role, RoleResource>
    {
        IStringLocalizer<GlobalResource> _globalLocalizer;

        public RoleValidator(IStringLocalizer<RoleResource> localizer,
            IStringLocalizer<GlobalResource> globalLocalizer) : base(localizer)
        {
            _globalLocalizer = globalLocalizer;

        }

        #region Insert Rules

        public override void LoadPreInsertRules()
        {

        }

        public override void LoadPostInsertRules()
        {

        }

        #endregion

        #region Update Rules

        public override void LoadPreUpdateRules()
        {

        }

        public override void LoadPostUpdateRules()
        {

        }

        #endregion

        #region Delete Rules

        public override void LoadPreDeleteRules()
        {

        }

        public override void LoadPostDeleteRules()
        {

        }

        #endregion

        #region Advanced Validations



        #endregion
    }

    #endregion

}

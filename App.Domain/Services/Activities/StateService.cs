﻿
using App.Common.Classes.DTO.Activities;
using App.Common.Resources;
using App.Common.Resources.Activities;
using App.Infrastructure.database.Entities;
using App.Infrastructure.Repositories.EntityFramework.Activities;
using AutoMapper;
using Doctus.Base.Cache;
using Doctus.Base.Services;
using Doctus.Validator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain.Services.Activities
{
    public interface IStateService : IBaseService<StateDTO>
    {

    }
    public class StateService : BaseService<StateDTO, State>, IStateService
    {
        private IStateRepository _ActivityRepository;

        public StateService(IStateRepository repository, IMemoryCacheManager memoryCache,
            IMapper mapper, IServiceValidator<State> validation, IConfiguration configuration, IRedisCache Cache)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
            _ActivityRepository = repository;

        }


    }

    #region Validator

    public class StateValidator : BaseServiceValidator<State, StateResource>
    {
        IStringLocalizer<GlobalResource> _globalLocalizer;

        public StateValidator(IStringLocalizer<StateResource> localizer,
            IStringLocalizer<GlobalResource> globalLocalizer) : base(localizer)
        {
            _globalLocalizer = globalLocalizer;

        }

        #region Insert Rules

        public override void LoadPreInsertRules()
        {

        }

        public override void LoadPostInsertRules()
        {

        }

        #endregion

        #region Update Rules

        public override void LoadPreUpdateRules()
        {

        }

        public override void LoadPostUpdateRules()
        {

        }

        #endregion

        #region Delete Rules

        public override void LoadPreDeleteRules()
        {

        }

        public override void LoadPostDeleteRules()
        {

        }

        #endregion

        #region Advanced Validations



        #endregion
    }

    #endregion

}

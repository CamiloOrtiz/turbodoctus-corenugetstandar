﻿using App.Common.Classes.DTO.Activities;
using App.Common.Classes.Log;
using App.Common.Resources;
using App.Common.Resources.Activities;
using App.Infrastructure.database.Entities;
using App.Infrastructure.Repositories.EntityFramework.Activities;
using AutoMapper;
using Doctus.Base.Services;
using Doctus.Validator;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;
using System.Collections.Generic;
using Doctus.Base.Cache;

namespace App.Domain.Services.Activities
{
    public interface IActivityService : IBaseService<ActivityDTO>
    {
        Task<ActivityDTO> CreateAsync(ActivityDTO dTO, bool AllowCache);
    }

    public class ActivityService : BaseService<ActivityDTO, Activity>, IActivityService
    {
        private IActivityRepository _ActivityRepository;
        private IRollbarhelp _rollbar;
        private IMemoryCacheManager _memoryCacheManager;

        public ActivityService(IActivityRepository repository, IMemoryCacheManager memoryCacheManager,
            IMapper mapper, IServiceValidator<Activity> validation, IConfiguration configuration, IRollbarhelp rollbarhelp)
            : base(repository, memoryCacheManager, mapper, validation, configuration)
        {
            _ActivityRepository = repository;
            _rollbar = rollbarhelp;
            _memoryCacheManager = memoryCacheManager;
        }

        public async override Task<IEnumerable<ActivityDTO>> GetAllAsync()
        {
            var get = _mapperDependency.Map<List<ActivityDTO>>(_repository.GetAll());

            var storevalue = _memoryCacheManager.FindRedisCacheByKey(_cacheKey);
            List<ActivityDTO> list;
            if (!string.IsNullOrEmpty(storevalue))
            {
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivityDTO>>(storevalue);
                return list;
            }

            list = _mapperDependency.Map<List<ActivityDTO>>(_repository.GetAll());
            _memoryCacheManager.AddRedisCache(_cacheKey, Newtonsoft.Json.JsonConvert.SerializeObject(list));
            return list;
        }

        public async Task<ActivityDTO> CreateAsync(ActivityDTO dto, bool AllowCache = true)
        {

            Activity entity = _mapperDependency.Map<Activity>(dto);
            //Prevalidacion
            _validator.PreInsert(entity);

            entity = await _repository.CreateAsync(entity);
            //PostValidacion
            _validator.PostInsert(entity);
            if (AllowCache)
            {
                _rollbar.InsertError("Prueba");
            }
            //_cache.Remove(_cacheKey);
            return _mapperDependency.Map<ActivityDTO>(entity);
            //TO-DO: Log
        }

    }

    #region Validator

    public class ActivityValidator : BaseServiceValidator<Activity, ActivityResource>
    {
        IStringLocalizer<GlobalResource> _globalLocalizer;

        public ActivityValidator(IStringLocalizer<ActivityResource> localizer,
            IStringLocalizer<GlobalResource> globalLocalizer) : base(localizer)
        {
            _globalLocalizer = globalLocalizer;

        }

        #region Insert Rules

        public override void LoadPreInsertRules()
        {
            PreInsertValidator
                .RuleFor(c => c.Description).NotNull().NotEmpty()
                .WithMessage(c => string.Format(_globalLocalizer["PropertyNotNullMessage"], "Descripción"));


            PreInsertValidator
                .RuleFor(c => c.StateId).NotNull().NotEmpty()
                .WithMessage(c => string.Format(_globalLocalizer["PropertyNotNullMessage"], "Estado"));

            PreInsertValidator
                .RuleFor(c => c.Title).NotNull().NotEmpty()
                .WithMessage(c => string.Format(_globalLocalizer["PropertyNotNullMessage"], "Titulo"));

            PreInsertValidator
                .RuleFor(c => c.UserId).NotNull().NotEmpty()
                .WithMessage(c => string.Format(_globalLocalizer["PropertyNotNullMessage"], "Usuario"));

        }

        public override void LoadPostInsertRules()
        {

        }

        #endregion

        #region Update Rules

        public override void LoadPreUpdateRules()
        {
            PreUpdateValidator
                .RuleFor(c => c.Description).NotNull().NotEmpty()
                .WithMessage(c => string.Format(_globalLocalizer["PropertyNotNullMessage"], "Descripción"));
        }

        public override void LoadPostUpdateRules()
        {

        }

        #endregion

        #region Delete Rules

        public override void LoadPreDeleteRules()
        {
            PreDeleteValidator
              .RuleFor(c => c.ActivityId).NotNull().NotEmpty()
              .WithMessage(c => string.Format(_globalLocalizer["PropertyNotNullMessage"], "Descripción"));
        }

        public override void LoadPostDeleteRules()
        {

        }

        #endregion

        #region Advanced Validations



        #endregion
    }

    #endregion

}

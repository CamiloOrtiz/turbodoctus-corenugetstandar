﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using App.Config.Dependencies;

namespace App.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
            var host = BuildWebHost(args);
            Container.CreateDatabase(host);
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}

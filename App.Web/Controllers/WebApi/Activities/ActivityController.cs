﻿using System.Threading.Tasks;
using App.Common.Classes.DTO.Activities;
using App.Domain.Services.Activities;
using Doctus.Base.WebApi;
using Doctus.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Doctus.Extensions;
using System.Net;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using System;

namespace App.Web.Controllers.WebApi.Activities
{

    [Route("api/Activity")]
    public class ActivityController : BaseController<ActivityDTO>
    {
        protected IActivityService _ActivityService;
        protected IStringLocalizer<GlobalResource> _globalLocalizer;
        protected IConfiguration _configuration;

        public ActivityController(IActivityService ActivityService, IStringLocalizer<GlobalResource> globalLocalizer, IConfiguration configuration)
        : base(ActivityService, globalLocalizer)
        {
            _ActivityService = ActivityService;
            _globalLocalizer = globalLocalizer;
            _configuration = configuration;
        }

        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme,Roles = "Admin")]
        [HttpGet]
        [Route("getAllAsync")]
        public override async Task<IActionResult> Index()
        {
            try
            {
                var data = await _ActivityService.GetAllAsync();
                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));

            }
            catch (Exception ex)
            {

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError, _globalLocalizer["DefaultError"]));

            }
        }

        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        [HttpPost]
        [Route("createCache")]
        public async Task<IActionResult> CreateCache([FromBody] ActivityDTO dto)
        {
            try
            {
                var data = await _ActivityService.CreateAsync(dto,true);
                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
            }
            catch (Exception ex)
            {

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError, _globalLocalizer["DefaultError"]));

            }
        }

        [HttpPost]
        [Route("create")]
        public override IActionResult Create([FromBody] ActivityDTO modelDTO)
        {
            return base.Create(modelDTO);
        }

    }



}
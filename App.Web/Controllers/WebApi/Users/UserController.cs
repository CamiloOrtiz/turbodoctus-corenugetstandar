﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using App.Common.Classes.DTO.Users;
using App.Common.Resources.Users;
using App.Domain.Services.Users;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using Doctus.Base.WebApi;
using Doctus.Exceptions;
using Doctus.Extensions;
using Doctus.Resources;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using OpenIddict.Core;

namespace App.Web.Controllers.WebApi.Users
{
    [Route("api/User")]
    public class UserController : BaseController<UserDTO>
    {
        protected IUserService _UserService;
        protected IStringLocalizer<GlobalResource> _globalLocalizer;
        protected IConfiguration _configuration;
        private readonly IStringLocalizer<UserResource> _localizer;
        public UserController(IUserService UserService, IStringLocalizer<GlobalResource> globalLocalizer, IConfiguration configuration,
            IStringLocalizer<UserResource> localizer)
    : base(UserService, globalLocalizer)
        {
            _UserService = UserService;
            _globalLocalizer = globalLocalizer;
            _configuration = configuration;
            _localizer = localizer;
        }

        [HttpPost]
        [Route("Login")]
        public virtual async Task<IActionResult> Login([FromBody]OpenIdConnectRequest user)
        {
            try
            {
                var data = await _UserService.Login(user, $"{ this.Request.Scheme}://{this.Request.Host}");
                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
            }
            catch (Exception ex)
            {
                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError, _globalLocalizer["DefaultError"]));
            }
        }

        public override async Task<IActionResult> CreateAsync([FromBody] UserDTO modelDTO)
        {
            var data = await _UserService.CreateUserAsync(modelDTO);
            return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
        }

        // POST: connect/token
        [HttpPost("~/connect/token")]
        public async Task<IActionResult> Exchange(OpenIdConnectRequest request)
        {
            if (request.IsPasswordGrantType())
            {
                var user = await _UserService.GetUserIdentityByEmailAsync(request.Username) ?? await _UserService.GetUserIdentityByUserNameAsync(request.Username);
                if (user == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.InvalidGrant]
                    });
                }


                var result = await _UserService.Login(request, $"{ this.Request.Scheme}://{this.Request.Host}");
                if (result == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.AccessDenied,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.AccessDenied]
                    });
                }

                // Reject the token request if two-factor authentication has been enabled by the user.
                if (result == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.RequestNotSupported,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.RequestNotSupported]
                    });
                }

                // Ensure the user is allowed to sign in.
                if (result == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.UnauthorizedClient,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.UnauthorizedClient]
                    });
                }

                if (result == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.InvalidGrant]
                    });
                }

                //else
                //{
                //    var result = await _signInManager.SignInAsync(()
                //}               


                // Ensure the user is not already locked out.


                // Create a new authentication ticket.
                var ticket = await CreateTicketAsync(request);
                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }
            else if (request.IsRefreshTokenGrantType())
            {
                // Retrieve the claims principal stored in the refresh token.
                var info = await HttpContext.AuthenticateAsync(OpenIdConnectServerDefaults.AuthenticationScheme);

                // Retrieve the user profile corresponding to the refresh token.
                // Note: if you want to automatically invalidate the refresh token
                // when the user password/roles change, use the following line instead:
                // var user = _signInManager.ValidateSecurityStampAsync(info.Principal);
                var user = await _UserService.GetUserAsync(info.Principal);
                if (user == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.UnsupportedTokenType,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.UnsupportedTokenType]
                    });
                }

                // Ensure the user is still allowed to sign in.
                if (!await _UserService.CanSignInAsync(request))// _signInManager.CanSignInAsync(user)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidClient,
                        ErrorDescription = this._localizer[OpenIdConnectConstants.Errors.InvalidClient]
                    });
                }
                // Create a new authentication ticket, but reuse the properties stored
                // in the refresh token, including the scopes originally granted.
                var ticket = await CreateTicketAsync(request);
                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }
            return BadRequest(new OpenIdConnectResponse
            {
                Error = OpenIdConnectConstants.Errors.UnsupportedGrantType,
                ErrorDescription = "The specified grant type is not supported"
            });
        }

        //Generar Ticket

        private async Task<AuthenticationTicket> CreateTicketAsync(OpenIdConnectRequest request)
        {
            // Create a new ClaimsPrincipal containing the claims that
            // will be used to create an id_token, a token or a code.
            var principal = await _UserService.CreateUserPrincipalAsync(request);//await _signInManager.CreateUserPrincipalAsync(user);

            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), OpenIdConnectServerDefaults.AuthenticationScheme);


            //if (!request.IsRefreshTokenGrantType())
            //{
            // Set the list of scopes granted to the client application.
            // Note: the offline_access scope must be granted
            // to allow OpenIddict to return a refresh token.
            ticket.SetScopes(new[]
            {
                    OpenIdConnectConstants.Scopes.OpenId,
                    OpenIdConnectConstants.Scopes.Email,
                    OpenIdConnectConstants.Scopes.Phone,
                    OpenIdConnectConstants.Scopes.Profile,
                    OpenIdConnectConstants.Scopes.OfflineAccess,
                    OpenIddictConstants.Scopes.Roles
            }.Intersect(request.GetScopes()));
            //}

            ticket.SetResources(request.GetResources());

            // Note: by default, claims are NOT automatically included in the access and identity tokens.
            // To allow OpenIddict to serialize them, you must attach them a destination, that specifies
            // whether they should be included in access tokens, in identity tokens or in both.

            foreach (var claim in ticket.Principal.Claims)
            {
                // Never include the security stamp in the access and identity tokens, as it's a secret value.
                //if (claim.Type == IdentityOptions.Value.ClaimsIdentity.SecurityStampClaimType)
                //    continue;


                var destinations = new List<string> { OpenIdConnectConstants.Destinations.AccessToken };

                // Only add the iterated claim to the id_token if the corresponding scope was granted to the client application.
                // The other claims will only be added to the access_token, which is encrypted when using the default format.
                if ((claim.Type == OpenIdConnectConstants.Claims.Subject && ticket.HasScope(OpenIdConnectConstants.Scopes.OpenId)) ||
                    (claim.Type == OpenIdConnectConstants.Claims.Name && ticket.HasScope(OpenIdConnectConstants.Scopes.Profile)) ||
                    (claim.Type == OpenIdConnectConstants.Claims.Role && ticket.HasScope(OpenIddictConstants.Claims.Roles)))
                {
                    destinations.Add(OpenIdConnectConstants.Destinations.IdentityToken);
                }


                claim.SetDestinations(destinations);
            }

            return ticket;
        }

        [HttpPost]
        [Route("FindByName")]
        public async Task<IActionResult> FindByName([FromBody] UserDTO user)
        {
            try
            {
                var data = await _UserService.FindByNameAsync(user);
                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
            }
            catch (Exception ex)
            {
                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError, _globalLocalizer["DefaultError"]));
            }

        }
    }
}
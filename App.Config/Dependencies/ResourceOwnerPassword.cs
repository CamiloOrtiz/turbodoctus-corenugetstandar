﻿using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.SecurityEntities;
using App.Infrastructure.Repositories.EntityFramework.Users;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using App.Common.Resources.Users;
using static IdentityModel.OidcConstants;

namespace App.Config.Dependencies
{
   public class ResourceOwnerPassword : IResourceOwnerPasswordValidator
    {
        UserManager<User> _userManager;
        public ResourceOwnerPassword(UserManager<User> userManager)
            : base()
        {
            _userManager = userManager;
        }
        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            
            var user = _userManager.FindByNameAsync(context.UserName).Result;
            if (user == null)
            {
                context.Result = new GrantValidationResult(TokenErrors.InvalidRequest, "Token invalid");
                return Task.FromResult(0);
            }
            context.Result = new GrantValidationResult(user.Id.ToString(), user.PasswordHash);
            return Task.FromResult(0);
        }
    }
}

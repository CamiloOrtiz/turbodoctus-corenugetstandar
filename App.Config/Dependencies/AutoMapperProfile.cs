﻿
using App.Common.Classes.DTO;
using App.Common.Classes.DTO.Activities;
using App.Common.Classes.DTO.Users;
using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.SecurityEntities;
using AutoMapper;
using Microsoft.Extensions.Configuration;

namespace App.Config.Dependencies
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<Role, RoleDTO>()
                .ForMember(x => x.Rolename, y => y.MapFrom(z => z.Name))
                .ReverseMap();
            CreateMap<State, StateDTO>().ReverseMap();
            CreateMap<Activity, ActivityDTO>().ReverseMap();
        }
    }
}

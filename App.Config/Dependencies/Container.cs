﻿using App.Common.Classes.Log;
using App.Domain.Services.Activities;
using App.Domain.Services.Users;
using App.Infrastructure.database.Entities;
using App.Infrastructure.Database.Models;
using App.Infrastructure.Database.SecurityEntities;
using App.Infrastructure.Repositories.EntityFramework.Activities;
using App.Infrastructure.Repositories.EntityFramework.Users;
using AspNet.Security.OpenIdConnect.Primitives;
using AutoMapper;
using Doctus.Base.Cache;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Rollbar.AspNetCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using static App.Domain.Services.Users.UserService;

namespace App.Config.Dependencies
{
    public class Container
    {


        public static void Register(IServiceCollection services, IConfiguration configuration)
        {

            #region Configuration



            services.AddRollbarLogger(loggerOptions =>
            {
                loggerOptions.Filter = (loggerName, loglevel) => loglevel >= LogLevel.Trace;
            });

            services.AddMemoryCache();
            services.AddAutoMapper();

            var configMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });

            var mapper = configMapper.CreateMapper();

            services.AddSingleton(mapper);

            // Context

            #region IdentityServer4

            var ApiName = configuration["Identity:ApiName"];
            var BaseAddress = configuration["Identity:BaseAddress"];

            services.AddScoped<Doctus_PlantillaNetCoreContext, Doctus_PlantillaNetCoreContext>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddIdentity<User, Role>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                //options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 2;
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            })
            .AddEntityFrameworkStores<Doctus_PlantillaNetCoreContext>()
            .AddErrorDescriber<TranslatedIdentityErrorDescriber>()
            .AddDefaultTokenProviders();

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(identityConfig.GetIdentityResources())
                .AddInMemoryApiResources(identityConfig.GetApiResources())
                .AddInMemoryClients(identityConfig.GetClients())
                .AddAspNetIdentity<User>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Authority = BaseAddress;
                options.Audience = ApiName;
                options.RequireHttpsMetadata = false;
            });


            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPassword>();
            services.AddTransient<IProfileService, IdentityClaimsProfileService>();

            #endregion


            ConfigureDatabase(services, configuration);
            #endregion

            #region IBaseCRUDRepository
            #endregion

            #region Interface
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IActivityRepository, ActivityRepository>();
            services.AddScoped<IStateRepository, StateRepository>();
            #endregion

            #region Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IActivityService, ActivityService>();
            services.AddScoped<IStateService, StateService>();
            services.AddSingleton<IMemoryCacheManager, MemoryCacheManager>();
            services.AddSingleton<IRedisCache, RedisCache>();
            services.AddSingleton<IRollbarhelp, Rollbarhelp>();
            #endregion

            #region Validators
            services.AddScoped<Doctus.Validator.IServiceValidator<User>, USerValidator>();
            services.AddScoped<Doctus.Validator.IServiceValidator<Role>, RoleValidator>();
            services.AddScoped<Doctus.Validator.IServiceValidator<State>, StateValidator>();
            services.AddScoped<Doctus.Validator.IServiceValidator<Activity>, ActivityValidator>();

            #endregion

            #region CorsPolicy

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            #endregion

            #region Resources

            // Location Configuration
            //services.AddLocalization(resource => resource.ResourcesPath = "Ruta");

            services.Configure<RequestLocalizationOptions>(opts =>
            {
                string english = "en-US";
                string spanish = "es";
                string spanishColombia = "es-CO";

                var supportedCultures = new List<CultureInfo> {
                    new CultureInfo(english),
                    new CultureInfo(spanish),
                    new CultureInfo(spanishColombia)
                };
                opts.DefaultRequestCulture = new RequestCulture(culture: english, uiCulture: english);
                opts.SupportedCultures = supportedCultures;
                opts.SupportedUICultures = supportedCultures;
            });


            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization();

            #endregion

        }

        public static void CreateDatabase(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<Doctus_PlantillaNetCoreContext>();
                    DbInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    throw ex;
                    //var logger = services.GetRequiredService<ILogger<app>>();
                    //logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }
        }

        private static void ConfigureDatabase(IServiceCollection services, IConfiguration configuration)
        {
            //configure database
            services.AddDbContext<Doctus_PlantillaNetCoreContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseSqlServer(configuration.GetConnectionString("AppDatabaseConnection"), x => { x.MigrationsAssembly("App.Infrastructure"); x.UseRowNumberForPaging(); });
            });

        }

    }
}

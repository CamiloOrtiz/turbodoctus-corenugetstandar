﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Constants
{
    public class RoleNames
    {
        public const string ADMINISTRATOR = "Administrador";
        public const string SALESMAN = "Vendedor";
        public const string EXECUTIVE = "Ejecutivo";

    }
}

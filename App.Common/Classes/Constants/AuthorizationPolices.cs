﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Constants
{
    public class AuthorizationPolicies
    {
        // Admin
        public const string Admin = "AdminPolicy";
        public const string Test = "test";
        public const string SalesMan = "SalesManPolicy";
        public const string AllUsers = "AllUserdPolicy";
        public const string Executive = "ExecutivePolicy";
        //public const string ReadUser = "ReadUsers";
        //public const string ReadUsers = "ReadUsers";
        //public const string UpdateUsers = "UpdateUsers";
        //public const string ResetPasswordUsers = "ResetPasswordUsers";
        //public const string DeleteUsers = "DeleteUsers";

        //// Campaigns
        //public const string CreateCampaigns = "CreateCampaigns";
        //public const string ReadCampaign = "ReadCampaign";
        //public const string ReadCampaigns = "ReadCampaigns";
        //public const string UpdateCampaigns = "UpdateCampaigns";
        //public const string DeleteCampaigns = "DeleteCampaigns";

        //// Allies
        //public const string CreateAllies = "CreateAllies";
        //public const string ReadAlly = "ReadAlly";
        //public const string ReadAllies = "ReadAllies";
        //public const string UpdateAllies = "UpdateAllies";
        //public const string DeleteAllies = "DeleteAllies";

        //// Payments
        //public const string CreatePayments = "CreatePayments";
        //public const string ReadPayment = "ReadPayment";
        //public const string ReadPayments = "ReadPayments";
        //public const string UpdatePayments = "UpdatePayments";
        //public const string DeletePayments = "DeletePayments";

        //// Brands
        //public const string CreateBrands = "CreateBrands";
        //public const string ReadBrand = "ReadBrand";
        //public const string ReadBrands = "ReadBrands";
        //public const string UpdateBrands = "UpdateBrands";
        //public const string DeleteBrands = "DeleteBrands";

        //// Categories
        //public const string CreateCategories = "CreateCategories";
        //public const string ReadCategory = "ReadCategory";
        //public const string ReadCategories = "ReadCategories";
        //public const string UpdateCategories = "UpdateCategories";
        //public const string DeleteCategories = "DeleteCategories";

        //// TypeInventories
        //public const string CreateTypeInventories = "CreateTypeInventories";
        //public const string ReadTypeInventory = "ReadTypeInventory";
        //public const string ReadTypeInventories = "ReadTypeInventories";
        //public const string UpdateTypeInventories = "UpdateTypeInventories";
        //public const string DeleteTypeInventories = "DeleteTypeInventories";

        //// Channels
        //public const string CreateChannels = "CreateChannels";
        //public const string ReadChannel = "ReadChannel";
        //public const string ReadChannels = "ReadChannels";
        //public const string UpdateChannels = "UpdateChannels";
        //public const string DeleteChannels = "DeleteChannels";

        //// TypePieces
        //public const string CreateTypePieces = "CreateTypePieces";
        //public const string ReadTypePiece = "ReadTypePiece";
        //public const string ReadTypePieces = "ReadTypePieces";
        //public const string UpdateTypePieces = "UpdateTypePieces";
        //public const string DeleteTypePieces = "DeleteTypePieces";

        //// Offers
        //public const string CreateOffers = "CreateOffers";
        //public const string ReadOffer = "ReadOffer";
        //public const string ReadOffers = "ReadOffers";
        //public const string UpdateOffers = "UpdateOffers";
        //public const string DeleteOffers = "DeleteOffers";
        //public const string ChangePositionOffers = "ChangePositionOffers";
        //public const string ReadPendingOffers = "ReadPendingOffers";
        //public const string ReadPendingOrInactiveOffers = "ReadPendingOrInactiveOffers";
        //public const string ReadLastActiveOffer = "ReadLastActiveOffer";
        //public const string PublishOffers = "PublishOffers";

    }
}

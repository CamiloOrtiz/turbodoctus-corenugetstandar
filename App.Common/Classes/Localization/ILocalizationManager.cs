﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Localization
{
    public interface ILocalizationManager
    {
        string GetStringResource(string classkey, string resourceKey);
        string GetStringResourceWithFormat(string classkey, string resourceKey, params string[] formatParams);
    }
}

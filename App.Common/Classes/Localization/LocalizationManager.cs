﻿using App.Common.Classes.Localization;
using Microsoft.Extensions.Localization;
using System;
using System.Web;

namespace App.Common.Localization
{
    public  class LocalizationManager<T> where T : class
    {
        IStringLocalizer<T> _localizer;

        public string GetLocalizedString(string key)
        {
            return _localizer[key];
        }
    }
}

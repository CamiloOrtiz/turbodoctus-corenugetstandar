﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Helpers
{
    public static class DateTimeHelper
    {
        /// <summary>
		/// Retorna la fecha y hora actual de Colombia
		/// y no la del servidor remoto
		/// </summary>
		/// <returns>Retorna la hora actual de Colombia</returns>
		public static DateTime GetCurrentColombianTime()
        {
            return DateTime.Now.ToUniversalTime().AddHours(-5);
        }

        public static DateTime FromUniversalTimeToColombianTime(this DateTime date)
        {
            return date.AddHours(-5);
        }

        public static DateTime ConvertFromUnixTimestamp(this long timestamp)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return epoch.AddSeconds(timestamp);
        }

        public static long ConvertToUnixTimestamp(this DateTime date)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date.ToUniversalTime() - epoch;
            return (long)Math.Floor(diff.TotalSeconds);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace App.Common.Classes.Helpers
{
    public static class SerializeHelper<T>
    {
        public static string ToJson(T data)
        {
            string result = string.Empty;
            if (data != null)
            {
                result = JsonConvert.SerializeObject(data);
            }

            return result;
        }
    }
}

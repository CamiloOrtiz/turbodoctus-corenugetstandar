﻿using App.Common.Classes.Constants;
using Doctus.DTO.Common;
using App.Common.Classes.Enums;
using System.Collections.Generic;
using System.Linq;

namespace App.Common.Classes.Helpers
{
    public static class FilterHelper
    {
        private static List<ResponseItemDTO> AllOperators()
        {
            var conditionList = new List<ResponseItemDTO>()
            {
                new ResponseItemDTO() { Id = OperatorEnum.Contains.ToString(), Value = GlobalConstants.CONDITION_CONTAINS },
                new ResponseItemDTO() { Id = OperatorEnum.EndsWith.ToString(), Value = GlobalConstants.CONDITION_ENDS_WITH },
                new ResponseItemDTO() { Id = OperatorEnum.Equals.ToString(), Value = GlobalConstants.CONDITION_EQUALS },
                new ResponseItemDTO() { Id = OperatorEnum.GreaterThan.ToString(), Value = GlobalConstants.CONDITION_GREATER_THAN },
                new ResponseItemDTO() { Id = OperatorEnum.GreaterThanOrEqual.ToString(), Value = GlobalConstants.CONDITION_GREATER_THAN_OR_EQUAL },
                new ResponseItemDTO() { Id = OperatorEnum.LessThan.ToString(), Value = GlobalConstants.CONDITION_LESS_THAN },
                new ResponseItemDTO() { Id = OperatorEnum.LessThanOrEqualTo.ToString(), Value = GlobalConstants.CONDITION_LESS_THAN_OR_EQUAL_TO },
                //new ResponseItemDTO() { Id = OperatorEnum.NotEqual.ToString(), Value = GlobalConstants.CONDITION_NOT_EQUAL },
                new ResponseItemDTO() { Id = OperatorEnum.StartsWith.ToString(), Value = GlobalConstants.CONDITION_STARTS_WITH }
            };

            return conditionList;
        }

        private static List<int> PageQuantity()
        {
            var itemList = new  List<int>();

            for (int i = 50; i < 2500; i += 50)
                itemList.Add(i);

            return itemList;
        }

        public static ResponseFilterDTO GetResponseFilter()
        {
            return new ResponseFilterDTO()
            {
                ConditionList = AllOperators().OrderBy(c => c.Value).ToList(),
                PageQuantityList = PageQuantity()
            };
        }
    }
}

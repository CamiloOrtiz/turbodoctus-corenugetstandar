﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App.Common.Classes.Helpers
{
	public class UrlHelper
	{
        private readonly IHostingEnvironment environment;

        public UrlHelper(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        public bool IsValidRedirectUrl(string url)
		{
			if (string.IsNullOrEmpty(url))
			{
				return false;
			}
			else
			{
				return ((url[0] == '/' && (url.Length == 1 ||
						(url[1] != '/' && url[1] != '\\'))) ||
						(url.Length > 1 &&
						 url[0] == '~' && url[1] == '/'));
			}
		}

        //public string GetBaseUrl()
        //{
        //    HttpRequest r = new HttpRequest();
        //    r.Scheme
        //    r.Host
        //    r.PathBase

        //    return environment.WebRootPath;
        //}
    }    
}
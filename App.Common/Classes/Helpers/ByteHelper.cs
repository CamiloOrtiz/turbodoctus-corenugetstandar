﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace App.Common.Classes.Helpers
{
    public static class ByteHelper
    {
        public static ByteArrayContent ParseDataToByte(object data)
        {
            var stringContent = JsonConvert.SerializeObject(data);
            var buffer = Encoding.UTF8.GetBytes(stringContent);

            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return byteContent;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace App.Common.Classes.Helpers
{
    public class ImageHelper
    {
        public static string ToBase64(Stream data)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                data.CopyTo(memoryStream);
                byte[] imageBytes = memoryStream.ToArray();
                return Convert.ToBase64String(imageBytes);
            }
        }

        public static MemoryStream FromBase64(string data)
        {
            byte[] imageBytes = Convert.FromBase64String(data);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            return ms;
        }
    }
}

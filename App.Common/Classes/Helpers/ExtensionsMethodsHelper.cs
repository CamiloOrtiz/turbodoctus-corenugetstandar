﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Helpers
{
    public static class ExtensionsMethodsHelper
    {
        public enum DoubleFormat
        {
            ShowDecimalsIfPresent,
            ShowNoDecimals
        }

        /// <summary>
        /// Formatea un numero decimal con dos decimales y punto cmo se
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string GetTruncatedValue(this decimal number)
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0.##}", (Math.Truncate(100m * number) / 100m));
        }

        /// <summary>
        /// Obtiene la version trucada amigable con el usuario de un texto. Por ejemplo:
        /// Si se tiene "texto muy largo" y se pasa como parametro '5' se obtendra "texto..."
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length">longituda maxima deseada incluyendo los 3 puntos suspensivos</param>
        /// <returns></returns>
        public static string ShortBeautyfulTo(this string text, int length)
        {
            if (text.Length > length && length > 3)
            {
                return text.Substring(0, length - 3) + "...";
            }
            return text;
        }

        public static string ToString(this decimal number, DoubleFormat format)
        {
            if (format == DoubleFormat.ShowDecimalsIfPresent)
            {
                return number.ToString("#.##");
            }

            if (format == DoubleFormat.ShowNoDecimals)
            {
                return number.ToString("#");
            }

            return number.ToString();
        }

        public static string ToString(this decimal? number, DoubleFormat format)
        {
            if (number.HasValue)
            {
                return number.Value.ToString(format);
            }
            return null;
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Helpers
{
    public interface IHttpHelper
    {
        string GetIpAdress();
    }

    public class HttpHelper : IHttpHelper
    {
        private IHttpContextAccessor _accessor;
        public HttpHelper(IHttpContextAccessor Accesor)
        {
            _accessor = Accesor;
        }

        public string GetIpAdress()
        {
            return _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }
    }
}

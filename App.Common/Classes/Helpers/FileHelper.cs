﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace App.Common.Classes.Helpers
{
    public static class FileHelper
    {
        public static string ToBase64(Stream data)
        {
            StreamReader reader = new StreamReader(data);

            byte[] bytedata = System.Text.Encoding.Default.GetBytes(reader.ReadToEnd());

            return Convert.ToBase64String(bytedata);
        }

        public static Stream ToStream(string base64Data)
        {
            byte[] streamBytes = Convert.FromBase64String(base64Data);
            MemoryStream ms = new MemoryStream(streamBytes, 0, streamBytes.Length);

            return ms;
        }
                            
    }
}

﻿using App.Common.Classes.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Helpers
{
    public static class EnumHelper
    {
        public static string GetEnumDescription(Enum tEnum)
        {
            return tEnum.GetDescription();
        }
    }
}

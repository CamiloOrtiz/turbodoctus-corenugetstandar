﻿
using Rollbar;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Log
{
    public interface IRollbarhelp
    {
        void InsertError(Exception Message);
        void InsertCritical(String message);
        void InsertDebug(String message);
        void InsertError(String message);
        void InsertInfo(String message);
        void InsertWarning(String message);


    }


    public class Rollbarhelp : IRollbarhelp
    {
        public Rollbarhelp() : base()
        {

        }

        public void InsertError(Exception Message)
        {
            RollbarLocator.RollbarInstance.Error(Message);
        }


        public void InsertCritical(String message)
        {
            RollbarLocator.RollbarInstance.Critical(message);
        }


        public void InsertDebug(String message)
        {
            RollbarLocator.RollbarInstance.Debug(message);
        }

        public void InsertError(String message)
        {
            RollbarLocator.RollbarInstance.Error(message);
        }


        public void InsertInfo(String message)
        {
            RollbarLocator.RollbarInstance.Info(message);
        }

        public void InsertWarning(String message)
        {
            RollbarLocator.RollbarInstance.Warning(message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Enums
{
    public enum OperatorEnum
    {
        Contains,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqualTo,
        StartsWith,
        EndsWith,
        Equals,
        //NotEqual
    }
}

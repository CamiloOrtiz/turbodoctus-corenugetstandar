﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace App.Common.Classes.DTO.Users
{
    [DataContract()]
   public class RoleDTO
    {
        [DataMember()]
        [UIHint("Hidden")]
        [Editable(false)]
        public int RoleId { get; set; }
        [DataMember()]
        public string Rolename { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace App.Common.Classes.DTO.Users
{
    [DataContract()]
   public class UserDTO
    {

        [DataMember()]
        [UIHint("Hidden")]
        [Editable(false)]
        public int UserId { get; set; }
        [DataMember()]
        public string UserName { get; set; }
        [DataMember()]
        public string PasswordHash { get; set; }
        [DataMember()]
        public string FirstName { get; set; }
        [DataMember()]
        public string LastName { get; set; }
        [DataMember()]
        public string Email { get; set; }
        //[DataMember()]
        //public string Address { get; set; }
        [DataMember()]
        public string PhoneNumber { get; set; }
        [DataMember()]
        public DateTime CreationDate { get; set; }
        [DataMember()]
        public DateTime? UpdateDate { get; set; }
        [DataMember()]
        public bool? IsBlocked { get; set; }
        [DataMember()]
        public IActionResult Token { get; set; }
        [DataMember()]
        public string RoleId { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }

    }
}

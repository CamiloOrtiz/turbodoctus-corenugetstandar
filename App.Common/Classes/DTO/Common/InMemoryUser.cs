﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Claims;

namespace App.Common.Classes.DTO.Common
{
    public class InMemoryUser
    {
        public InMemoryUser() { }

        public bool Enabled { get; set; }
        public string Subject { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public ICollection<Claim> Claims { get; set; }
    }
}

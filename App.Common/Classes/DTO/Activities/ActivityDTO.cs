﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace App.Common.Classes.DTO.Activities
{
    [DataContract()]
    public class ActivityDTO
    {
        [DataMember()]
        [UIHint("Hidden")]
        [Editable(false)]
        public int ActivityId { get; set; }
        [DataMember()]
        public string Description { get; set; }
        [DataMember()]
        public DateTime? CreationDate { get; set; }
        [DataMember()]
        public DateTime? UpdateDate { get; set; }
        [DataMember()]
        public string Title { get; set; }
        [DataMember()]
        public int? EstimatedHours { get; set; }
        [DataMember()]
        public int? UserId { get; set; }
        [DataMember()]
        public int? StateId { get; set; }
    }
}

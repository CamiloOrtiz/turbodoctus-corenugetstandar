﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace App.Common.Classes.DTO.Activities
{
    [DataContract()]
    public class StateDTO
    {
        [DataMember()]
        [UIHint("Hidden")]
        [Editable(false)]
        public int StateId { get; set; }
        [DataMember()]
        public string StateName { get; set; }
    }
}
